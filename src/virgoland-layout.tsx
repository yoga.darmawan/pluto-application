import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import App from './app';

import { store } from '@/shared/core/redux/store';
import { login, requestPermission } from '@/modules/auth/presentation/view-model/redux/action';
import { getToken as gT } from '@/shared/utils/local-storage';
import { isPermissionValid } from './shared/utils/permission/util';

const lifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: App,
  errorBoundary(err, info, props) {
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});

export const { bootstrap, mount, unmount } = lifecycles;

let initProgress = false;
let initFinish = false;

let queueCallbacks = [];

export const init = async (cb?: () => void) => {
  const { auth } = store.getState();
  if (!initProgress && !initFinish) {
    initProgress = true;
    const token = gT();
    if (token) {
      store.dispatch(login());
      store.dispatch(
        requestPermission(() => {
          if (cb) {
            cb();
          }
          queueCallbacks.forEach((queueCb) => queueCb());
          initProgress = false;
          initFinish = true;
          queueCallbacks = [];
        }),
      );
    } else {
      initProgress = false;
      initFinish = true;
      queueCallbacks = [];
    }
  } else if (initProgress && !initFinish) {
    queueCallbacks.push(cb);
  } else if (!initProgress && initFinish && auth.isLoggedin) {
    cb();
  }
};

init();

export const hasPermission = (permission: string) => {
  const { auth } = store.getState();
  if (auth.permissioin && auth.isLoggedin) {
    return isPermissionValid(auth.permission, permission, ['/']);
  }
  return false;
};

export const getToken = () => {
  return gT();
};
