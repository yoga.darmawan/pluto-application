export type PathName = 'ResetPassword' | 'HomeLogin';

export interface PathParams {
  url?: string;
  name?: PathName;
  query?: {
    [key: string]: string | number;
  };
  id?: string | number;
  pushType?: 'push' | 'replace';
}

export interface HTTPResponse<T = Record<string, never>> {
  status: number | string;
  message: string;
  data: T;
}
