import { HTTPResponse } from '../general/entity';
import {
  LoginPayload,
  LoginResponse,
  LogoutResponse,
  PermissionResponse,
  ResetPasswordPayload,
} from './entity';

export default interface AuthUseCase {
  requestLogin: (payload: LoginPayload) => Promise<LoginResponse>;
  requestLogout: () => Promise<LogoutResponse>;
  requestPermission: () => Promise<PermissionResponse>;
  requestResetPassword: (email: ResetPasswordPayload) => Promise<HTTPResponse>;
}
