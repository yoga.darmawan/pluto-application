export interface LoginPayload {
  user_name: string;
  password: string;
}

export interface LoginPermission {
  id: number;
  name: string;
}

export interface LoginResponse {
  data: {
    IDToken: string;
    UserID: string;
    Username: string;
    UserRole: {
      name: string;
      permissions: LoginPermission[];
    };
  };
  message: string;
}

export interface LogoutResponse {
  message: string;
}

export interface PermissionResponse {
  name: string;
  permission_list: string[];
  role: string;
  user_id: number;
  user_name: string;
}

export interface PermissionNode {
  children?: PermissionNode[];
  value?: string;
}

export interface ResetPasswordPayload {
  email: string;
}
