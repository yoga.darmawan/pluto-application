import {
  LoginPayload,
  LoginResponse,
  LogoutResponse,
  PermissionResponse,
  ResetPasswordPayload,
} from '@/domains/auth/entity';
import AuthRepository from '@/domains/auth/repository';
import AuthUseCase from '@/domains/auth/usecase';
import { HTTPResponse } from '@/domains/general/entity';

export default class AuthUseCaseImpl implements AuthUseCase {
  private repository: AuthRepository;

  constructor(repository: AuthRepository) {
    this.repository = repository;
  }

  async requestLogin(payload: LoginPayload): Promise<LoginResponse> {
    try {
      const response: LoginResponse = await this.repository.requestLogin(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestLogout(): Promise<LogoutResponse> {
    try {
      const response: LogoutResponse = await this.repository.requestLogout();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestPermission(): Promise<PermissionResponse> {
    try {
      const response: PermissionResponse = await this.repository.requestPermission();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestResetPassword(payload: ResetPasswordPayload): Promise<HTTPResponse> {
    try {
      const response = await this.repository.requestResetPassword(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }
}
