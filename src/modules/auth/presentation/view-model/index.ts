import { injectReducer } from '@/core/redux/store';
import authSlice from './redux/slice';
import dataModel from './redux/data';
import actionModel from './redux/action';

export default function AuthViewModel() {
  injectReducer('auth', authSlice.reducer);
  return {
    dataModel: () => dataModel(),
    actionModel: () => actionModel(),
  };
}
