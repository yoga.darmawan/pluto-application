import { PermissionNode } from '@/domains/auth/entity';

export interface InitialState {
  isLoggedin: boolean;
  permission: PermissionNode;
}
