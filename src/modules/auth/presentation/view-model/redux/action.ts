import { useDispatch } from 'react-redux';

import { AppThunk } from '@/shared/core/redux/store';
import { LoginPayload, ResetPasswordPayload } from '@/domains/auth/entity';
import Helper from '@/shared/core/redux/helper';
import { uiActions } from '@/shared/core/redux/ui-slice';
import { usecase } from '@/modules/auth/util';
import { authActions } from './slice';
import { removeToken, saveToken } from '@/shared/utils/local-storage';
import { convertPermissionArrayToPermissionTree } from '@/shared/utils/permission';

export function requestPermission(successCb?: (response: any) => void): AppThunk {
  return async (dispatch) => {
    try {
      dispatch(
        uiActions.triggerLoading({
          overlay: true,
        }),
      );
      const response = await usecase.requestPermission();
      const newPermission = response.permission_list.concat(['/']);
      dispatch(
        authActions.setPermission(convertPermissionArrayToPermissionTree([...newPermission])),
      );
      if (successCb) {
        successCb(response);
      }
    } catch (error: any) {
      // [NOTE]: Put some kind of logging activity here
    } finally {
      dispatch(
        uiActions.triggerLoading({
          overlay: false,
        }),
      );
    }
  };
}

export function login(): AppThunk {
  return async (dispatch) => {
    dispatch(authActions.setLogin(true));
  };
}

export default function ActionModel() {
  const dispatch = useDispatch();
  const { triggerLoading, ...helperAction } = Helper();

  function requestLogin(payload: LoginPayload, successCb?: (response: any) => void): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestLogin(payload);
        if (response.data.IDToken) {
          saveToken(response.data.IDToken);
          dispatch(requestPermission());
          dispatch(login());
        }
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  function requestLogout(successCb?: (response: any) => void): AppThunk {
    return async () => {
      try {
        triggerLoading({
          overlay: true,
        });
        const response = await usecase.requestLogout();
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        removeToken();
        window.location.href = '/';
        triggerLoading({
          overlay: false,
        });
      }
    };
  }

  function requestResetPassword(
    payload: ResetPasswordPayload,
    successCb?: (response: any) => void,
  ): AppThunk {
    return async () => {
      try {
        triggerLoading({
          primaryAction: true,
        });
        const response = await usecase.requestResetPassword(payload);
        if (successCb) {
          successCb(response);
        }
      } catch (error: any) {
        // [NOTE]: Put some kind of logging activity here
      } finally {
        triggerLoading({
          primaryAction: false,
        });
      }
    };
  }

  return {
    requestLogin: (payload: LoginPayload, successCb?: (response: any) => void) =>
      dispatch(requestLogin(payload, successCb)),

    requestLogout: (successCb?: (response: any) => void) => dispatch(requestLogout(successCb)),

    login: () => dispatch(login()),

    requestPermission: (successCb?: (response: any) => void) =>
      dispatch(requestPermission(successCb)),

    requestResetPassword: (payload: ResetPasswordPayload, successCb?: (response: any) => void) =>
      dispatch(requestResetPassword(payload, successCb)),

    ...helperAction,
  };
}
