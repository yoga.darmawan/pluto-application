import { useSelector } from 'react-redux';

import { RootState } from '@/shared/core/redux/store';
import DataModel from '@/shared/core/redux/data';

export default function AuthDataModel() {
  const dataModel = DataModel();
  const isLoggedin = useSelector((state: RootState) => state.auth.isLoggedin);
  const permission = useSelector((state: RootState) => state.auth.permission);

  return {
    isLoggedin,
    permission,
    ...dataModel,
  };
}
