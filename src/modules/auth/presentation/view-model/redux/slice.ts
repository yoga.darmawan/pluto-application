import { PermissionNode } from '@/domains/auth/entity';
import { createSlice } from '@reduxjs/toolkit';

import { InitialState } from './types';

const initAuth: InitialState = {
  isLoggedin: false,
  permission: {},
};

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    ...initAuth,
  },
  reducers: {
    setLogin(state, { payload }) {
      state.isLoggedin = payload;
    },
    setPermission(state, { payload }) {
      state.permission = payload as PermissionNode;
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice;
