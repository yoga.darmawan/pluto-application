import React from 'react';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/lab/LoadingButton';
import { useTheme } from '@emotion/react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';

import AuthViewModel from '@/modules/auth/presentation/view-model';
import { generatePath, useRedirect } from '@/shared/utils/router';
import VirgoLogo from '@/logos/virgo-full.png';

import { Wrapper, helperSx } from '../login/style';

const scheme = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Email is required'),
});

const ResetPassword = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel, dataModel } = auth;
  const action = actionModel();
  const {
    loading: { primaryAction },
  } = dataModel();
  // CUSTOM HOOKS
  const {
    palette: {
      primary: { main },
      common: { white },
    },
  } = useTheme();
  const { redirect } = useRedirect();

  const onReset = React.useCallback(({ email }) => {
    action.requestResetPassword(
      {
        email,
      },
      () => redirect({ name: 'HomeLogin' }),
    );
  }, []);

  const { handleSubmit, handleChange, handleBlur, values, errors, touched } = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: scheme,
    onSubmit: (payload) => {
      onReset(payload);
    },
  });

  return (
    <Wrapper>
      <div id="login" className="login">
        <div className="login_welcome">
          <h2>Forgot password ?</h2>
          <p className="lead">To reset your password, enter your virgoland registered email.</p>
        </div>
        <Paper elevation={3} className="login_card">
          <img src={VirgoLogo} alt="virgo" />
          <form className="login_form" onSubmit={handleSubmit} autoComplete="off">
            <TextField
              id="email-input"
              data-testid="email-input"
              name="email"
              className="login_email-input"
              variant="outlined"
              label="Email"
              size="small"
              autoComplete="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              error={errors.email && touched.email}
              helperText={errors.email && touched.email ? errors.email : null}
              FormHelperTextProps={{ sx: helperSx }}
              fullWidth
            />
            <div>
              <Link to={generatePath({ name: 'HomeLogin' })}>Back to login page</Link>
            </div>
            <div className="login_submit-wrapper">
              <Button
                sx={{
                  backgroundColor: main,
                  color: white,
                  '&:hover': { backgroundColor: main },
                }}
                loading={primaryAction}
                variant="outlined"
                type="submit"
              >
                Continue
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    </Wrapper>
  );
};

export default ResetPassword;
