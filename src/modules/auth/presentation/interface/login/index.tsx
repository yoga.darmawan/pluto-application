import React from 'react';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/lab/LoadingButton';
import { useTheme } from '@emotion/react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';

import AuthViewModel from '@/modules/auth/presentation/view-model';
import { generatePath } from '@/shared/utils/router';
import VirgoLogo from '@/logos/virgo-full.png';

import { Wrapper, helperSx } from './style';

const scheme = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Email is required'),
  password: Yup.string()
    .min(6, 'Must be longer than 6 characters')
    .max(32, 'Must be 32 characters or less')
    .required('Password is required'),
});

const Login = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel, dataModel } = auth;
  const action = actionModel();
  const {
    loading: { primaryAction },
  } = dataModel();
  // CUSTOM HOOKS
  const {
    palette: {
      primary: { main },
      common: { white },
    },
  } = useTheme();

  const onLogin = React.useCallback(({ email, password }) => {
    action.requestLogin({
      user_name: email,
      password,
    });
  }, []);

  const { handleSubmit, handleChange, handleBlur, values, errors, touched } = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: scheme,
    onSubmit: (payload) => {
      onLogin(payload);
    },
  });

  return (
    <Wrapper>
      <div id="login" className="login">
        <div className="login_welcome">
          <h2>Welcome back</h2>
          <p className="lead">Sign in to your account to continue</p>
        </div>
        <Paper elevation={3} className="login_card">
          <img src={VirgoLogo} alt="virgo" />
          <form className="login_form" onSubmit={handleSubmit} autoComplete="off">
            <TextField
              id="email-input"
              data-testid="email-input"
              name="email"
              className="login_email-input"
              variant="outlined"
              label="Email"
              size="small"
              autoComplete="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              error={errors.email && touched.email}
              helperText={errors.email && touched.email ? errors.email : null}
              FormHelperTextProps={{ sx: helperSx }}
              fullWidth
            />
            <TextField
              id="password-input"
              data-testid="password-input"
              name="password"
              className="login_password-input"
              variant="outlined"
              label="Password"
              size="small"
              type="password"
              autoComplete="current-password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              error={errors.password && touched.password}
              helperText={errors.password && touched.password ? errors.password : null}
              FormHelperTextProps={{ sx: helperSx }}
              fullWidth
            />
            <div>
              <Link to={generatePath({ name: 'ResetPassword' })}>Reset Password ?</Link>
            </div>
            <div className="login_submit-wrapper">
              <Button
                sx={{
                  backgroundColor: main,
                  color: white,
                  '&:hover': { backgroundColor: main },
                }}
                loading={primaryAction}
                variant="outlined"
                type="submit"
              >
                Sign In
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    </Wrapper>
  );
};

export default Login;
