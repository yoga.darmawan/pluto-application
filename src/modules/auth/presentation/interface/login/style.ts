import styled from '@emotion/styled';
import { SxProps } from '@mui/material';

export const Wrapper = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  .login {
    width: 410px;
  }

  .login_welcome {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .login_card {
    padding: 20px 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    img {
      width: 200px;
    }
  }

  .login_form {
    width: 100%;
  }

  .login_email-input,
  .login_password-input {
    margin-bottom: 1rem;
  }

  .login_reset-password {
    color: rgba(0, 0, 0, 0.54);
    font-size: 0.8rem;
    line-height: 1;
    letter-spacing: 0.00938em;
    cursor: pointer;
    font-weight: 700;
  }

  .login_submit-wrapper {
    display: flex;
    justify-content: center;
    margin-top: 2rem;
  }

  a {
    color: ${({ theme: { palette } }) => palette.common.black};
    text-decoration: none;
  }
`;

export const helperSx: SxProps = {
  marginTop: 0,
  fontWeight: 700,
  fontSize: '0.8rem',
};
