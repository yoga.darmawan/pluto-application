import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { render, cleanup, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ThemeProvider } from '@/theme/index';
import { store } from '@/core/redux/store';

import Login from '../index';

jest.mock('src/assets/img/logo/logo.png', () => '');

const requestLoginMock = jest.fn();

jest.mock('@/modules/auth/presentation/view-model/redux/action', () => () => {
  const actual = jest.requireActual('@/modules/auth/presentation/view-model/redux/action');
  return {
    ...actual.default(),
    requestLogin: requestLoginMock,
  };
});

const Component = (
  <ReduxProvider store={store}>
    <Login />
  </ReduxProvider>
);

const fillInput = async (testid: RegExp, value: string) => {
  const input = screen.getByLabelText(testid);
  await userEvent.type(input, value);
};

describe('Sign In Component Test Group', () => {
  afterEach(() => {
    cleanup();
  });
  beforeEach(() => {
    Object.defineProperty(window, 'location', {
      writable: true,
      value: { assign: jest.fn() },
    });
  });
  // 1. Email Input Check
  it('1. Email Input Check', async () => {
    // unit test begin
    render(Component, { wrapper: ThemeProvider });
    const emailInput = screen.getByTestId('email-input');
    expect(emailInput).toBeTruthy();
  });
  // 2. Password Check
  it('2. Password Check', () => {
    // unit test begin
    render(Component, { wrapper: ThemeProvider });
    const emailInput = screen.getByTestId('password-input');
    expect(emailInput).toBeTruthy();
  });
  // 3. Check Email Input Validation
  it('3. Check Email Input Validation', async () => {
    // unit test begin
    render(Component, { wrapper: ThemeProvider });
    const labelText = /email/i;
    const requiredText = /email is required/i;
    const invalidText = /invalid email address/i;
    const button = screen.getByText(/sign in/i, { selector: 'button' });

    await userEvent.click(button);
    const error1 = await screen.findByText(requiredText);
    expect(error1).toBeTruthy();

    await fillInput(labelText, 'ssssss');
    const error2 = await screen.findByText(invalidText);
    expect(error2).toBeTruthy();
    await userEvent.clear(screen.getByLabelText(labelText));

    await fillInput(labelText, 'yoga@yoga.com');
    const error3 = screen.queryByText(requiredText);
    const error4 = screen.queryByText(invalidText);
    expect(error3).not.toBeTruthy();
    expect(error4).not.toBeTruthy();
  });
  // 4. Check Password Input Validation
  it('4. Check Password Input Validation', async () => {
    // unit test begin
    render(Component, { wrapper: ThemeProvider });
    const labelText = /password/i;
    const requiredText = /password is required/i;
    const lowerLimitTest = /must be longer than 6 characters/i;
    const higherLimitTest = /be 32 characters or less/i;
    const button = screen.getByText(/sign in/i, { selector: 'button' });

    await userEvent.click(button);
    const error1 = await screen.findByText(requiredText);
    expect(error1).toBeTruthy();

    await fillInput(labelText, 'sssss');
    const error2 = await screen.findByText(lowerLimitTest);
    expect(error2).toBeTruthy();
    await userEvent.clear(screen.getByLabelText(labelText));

    await fillInput(labelText, 'ssssssssssssssssssssssssssssssssssssssssssssssssssssss');
    const error3 = await screen.findByText(higherLimitTest);
    expect(error3).toBeTruthy();
    await userEvent.clear(screen.getByLabelText(labelText));

    await fillInput(labelText, 'password');
    const error4 = screen.queryByText(requiredText);
    const error5 = screen.queryByText(lowerLimitTest);
    const error6 = screen.queryByText(higherLimitTest);
    expect(error4).not.toBeTruthy();
    expect(error5).not.toBeTruthy();
    expect(error6).not.toBeTruthy();
  });
  // 5. Check Login
  it('5. Check Login', async () => {
    // unit test begin
    render(Component, { wrapper: ThemeProvider });
    const emailLabelText = /email/i;
    const passwordLabelText = /password/i;
    const button = screen.getByText(/sign in/i, { selector: 'button' });

    await fillInput(emailLabelText, 'yoga@yoga.com');
    await fillInput(passwordLabelText, 'password');

    await userEvent.click(button);

    expect(requestLoginMock).toBeCalled();
  });
});
