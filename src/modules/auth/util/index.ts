import AuthUseCaseImpl from '../usecase/usecaseImpl';
import AuthRepositoryImpl from '../repository/repositoryImpl';
import AuthFetcher from '../repository/fetcher';

export const fetcher = new AuthFetcher();
export const repository = new AuthRepositoryImpl(fetcher);
export const usecase = new AuthUseCaseImpl(repository);
