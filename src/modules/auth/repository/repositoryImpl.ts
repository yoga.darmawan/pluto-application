import {
  LoginPayload,
  LoginResponse,
  LogoutResponse,
  PermissionResponse,
  ResetPasswordPayload,
} from '@/domains/auth/entity';
import AuthRepository from '@/domains/auth/repository';
import { HTTPResponse } from '@/domains/general/entity';

import AuthFetcher from './fetcher';

export default class AuthRepositoryImpl implements AuthRepository {
  private fetcher: AuthFetcher;

  constructor(fetcher: AuthFetcher) {
    this.fetcher = fetcher;
  }

  async requestLogin(payload: LoginPayload): Promise<LoginResponse> {
    try {
      const response = await this.fetcher.requestLogin(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestLogout(): Promise<LogoutResponse> {
    try {
      const response = await this.fetcher.requestLogout();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestPermission(): Promise<PermissionResponse> {
    try {
      const response = await this.fetcher.requestPermission();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestResetPassword(payload: ResetPasswordPayload): Promise<HTTPResponse> {
    try {
      const response = await this.fetcher.requestResetPassword(payload);
      return response;
    } catch (error) {
      throw error;
    }
  }
}
