import { Fetcher, FetcherHelper } from '@/core/fetcher';
import {
  LoginPayload,
  LoginResponse,
  LogoutResponse,
  PermissionResponse,
  ResetPasswordPayload,
} from '@/domains/auth/entity';
import { HTTPResponse } from '@/domains/general/entity';
import { getToken } from '@/shared/utils/local-storage';

const { createAxios, clientErrorHandler, clientSuccessHandler } = FetcherHelper;

export default class AuthFetcher extends Fetcher {
  constructor() {
    super();
    this.setFetcher(createAxios('').instance);
    this.setSuccessHandler(clientSuccessHandler);
    this.setErrorHandler(clientErrorHandler);
  }

  async requestLogin(data: LoginPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '').instance);
      this.setData(data);
      this.setUrl('/r/user/login');
      this.setQueryParam(null);
      const response: LoginResponse = await this.post();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestLogout() {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('/r/user/logout');
      this.setQueryParam(null);
      const response: LogoutResponse = await this.post();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestPermission() {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '', `Bearer ${getToken()}`).instance);
      this.setUrl('r/user/permissions');
      this.setQueryParam(null);
      const response: PermissionResponse = await this.get();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async requestResetPassword(payload: ResetPasswordPayload) {
    try {
      this.setFetcher(createAxios(process.env.BASE_API_URL || '').instance);
      this.setUrl('r/user/password-reset');
      this.setQueryParam(null);
      this.setData(payload);
      const response: HTTPResponse = await this.post();
      return response;
    } catch (error) {
      throw error;
    }
  }
}
