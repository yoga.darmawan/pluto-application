import React from 'react';

import Header from '@/shared/components/header';
import Sidebar from '@/shared/components/sidebar';

const PageLayout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <Sidebar />
      {children}
    </>
  );
};

export default PageLayout;
