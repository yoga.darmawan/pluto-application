import React from 'react';
import { useTheme } from '@emotion/react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import AuthViewModel from '@/modules/auth/presentation/view-model';

const Header = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel, dataModel } = auth;
  const { triggerSidebar } = actionModel();
  const {
    sidebar: { open },
  } = dataModel();
  // CUSTOM HOOKS
  const {
    customComponent: {
      sidebar: { width },
    },
  } = useTheme();

  return (
    <Box
      data-testid={open ? 'shrinked' : 'streched'}
      sx={{ flexGrow: 1, position: 'fixed', top: 0, left: open ? width : 0, right: 0 }}
    >
      <AppBar position="static">
        <Toolbar>
          {!open && (
            <IconButton
              data-testid="toggle-sidebar"
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={() => triggerSidebar({ open: true })}
            >
              <MenuIcon />
            </IconButton>
          )}
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Virgoland
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
