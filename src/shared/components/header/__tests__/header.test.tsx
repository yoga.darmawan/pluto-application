import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { render, fireEvent, cleanup, screen, act } from '@testing-library/react';

import { ThemeProvider } from '@/theme/index';
import { store } from '@/core/redux/store';
import { uiActions } from '@/shared/core/redux/ui-slice';

import Header from '../index';

const Component = (
  <ReduxProvider store={store}>
    <Header />
  </ReduxProvider>
);

describe('Sidebar Component Test Group', () => {
  afterEach(() => {
    cleanup();
  });
  // 1. Component Mount
  it('1. Component Mount', async () => {
    const wrapper = render(Component, { wrapper: ThemeProvider });
    expect(wrapper).toBeTruthy();
  });
  // 2. Streched Header
  it('2. Streched Header', async () => {
    render(Component, { wrapper: ThemeProvider });
    store.dispatch(
      uiActions.triggerSidebar({
        open: true,
      }),
    );
    await act(async () => {
      const headerState = await screen.findByTestId('shrinked');
      expect(headerState).toBeTruthy();
      const queryToggle = screen.queryByTestId('toggle-sidebar');
      expect(queryToggle).not.toBeTruthy();
    });
  });
  // 3. Shrinked Header by Toggling Sidebar
  it('3. Shrinked Header by Toggling Sidebar', async () => {
    render(Component, { wrapper: ThemeProvider });
    store.dispatch(
      uiActions.triggerSidebar({
        open: false,
      }),
    );
    await act(async () => {
      const queryToggle = await screen.findByTestId('toggle-sidebar');
      expect(queryToggle).toBeTruthy();
      const headerState = await screen.findByTestId('streched');
      expect(headerState).toBeTruthy();
      fireEvent.click(queryToggle);
    });
    await act(async () => {
      const headerState = await screen.findByTestId('shrinked');
      expect(headerState).toBeTruthy();
    });
  });
});
