import React from 'react';
import { Link } from 'react-router-dom';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import List from '@mui/material/List';
import MUIListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ExpandMore from '@mui/icons-material/ExpandMore';
import ExpandLess from '@mui/icons-material/ExpandLess';
import Collapse from '@mui/material/Collapse';
import LogoutIcon from '@mui/icons-material/Logout';
import ButtonBase from '@mui/material/ButtonBase';

import AuthViewModel from '@/modules/auth/presentation/view-model';
import { Menu } from '@/shared/core/redux/types';
import { usePermission } from '@/shared/utils/permission';
import VirgoLogo from '@/logos/virgo-full.png';

import VirgolandMenu from './menu';
import {
  toolbarSx,
  listItemTextSx,
  listIconChevronSx,
  listIconSx,
  listChildButtonSx,
  drawerPaperSx,
  LogoutRow,
  listSx,
  listChildItemSx,
  listItemSx,
  listChildSx,
} from './style';

export const ListItem = ({
  menu: { path, name, icon, children, key },
  findPermission,
}: {
  menu: Menu;
  findPermission: (string) => boolean;
}) => {
  const [open, setOpen] = React.useState(false);
  return (
    <>
      {findPermission(key) && (
        <>
          <MUIListItem sx={listItemSx}>
            <ListItemButton
              component={!children ? Link : null}
              to={path}
              onClick={() => setOpen(!open)}
            >
              <ListItemIcon sx={listIconSx}>{icon}</ListItemIcon>
              <ListItemText primary={name} primaryTypographyProps={listItemTextSx} />
              <ListItemIcon sx={listIconChevronSx}>
                {open ? <ExpandLess /> : <ExpandMore />}
              </ListItemIcon>
            </ListItemButton>
          </MUIListItem>
          {children && (
            <Collapse in={open}>
              <List sx={listChildSx}>
                {children.map(({ path: childPath, name: childName, key: childKey }) => (
                  <React.Fragment key={childKey}>
                    {findPermission(childKey) && (
                      <MUIListItem sx={listChildItemSx}>
                        <ListItemButton sx={listChildButtonSx} component={Link} to={childPath}>
                          <ListItemText primary={childName} />
                        </ListItemButton>
                      </MUIListItem>
                    )}
                  </React.Fragment>
                ))}
              </List>
            </Collapse>
          )}
        </>
      )}
    </>
  );
};

const Sidebar = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel, dataModel } = auth;
  const { triggerSidebar, requestLogout } = actionModel();
  const {
    sidebar: { open },
  } = dataModel();
  // CUSTOM HOOKS
  const { findPermission } = usePermission();
  return (
    <Drawer
      variant="permanent"
      anchor="left"
      open={open}
      data-testid={open ? 'open' : 'hidden'}
      PaperProps={{
        sx: {
          ...drawerPaperSx,
          width: (theme: any) => (open ? `${theme.customComponent.sidebar.width}px` : '0px'),
        },
      }}
    >
      <Toolbar sx={toolbarSx}>
        <img src={VirgoLogo} alt="virgo" />
        <IconButton id="hide" data-testid="hide" onClick={() => triggerSidebar({ open: false })}>
          <ChevronLeftIcon />
        </IconButton>
      </Toolbar>
      <Divider />
      <List component="nav" sx={listSx}>
        {VirgolandMenu.map((menu) => (
          <ListItem key={menu.key} menu={menu} findPermission={findPermission} />
        ))}
      </List>
      <LogoutRow>
        <ButtonBase component="div" onClick={() => requestLogout()}>
          <LogoutIcon />
          <span>Logout</span>
        </ButtonBase>
      </LogoutRow>
    </Drawer>
  );
};

export default Sidebar;
