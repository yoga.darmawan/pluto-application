import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { render, fireEvent, cleanup, screen, act } from '@testing-library/react';

import { ThemeProvider } from '@/theme/index';
import { store } from '@/core/redux/store';
import { authActions } from '@/modules/auth/presentation/view-model/redux/slice';
import { convertPermissionArrayToPermissionTree } from '@/shared/utils/permission';

import SideBar from '../index';

const permissionMock = [
  'customer.customer-details.customer-block',
  'customer.customer-details.customer-unblock',
  'customer.customer-details.device-block',
  'customer.customer-details.device-unblock',
  'customer.customer-details.partner-linkage-block',
  'customer.customer-details.partner-linkage-unblock',
  'customer.customer-details.list',
];

const requestLogoutMock = jest.fn();

jest.mock('@/logos/virgo-full.png', () => '');

jest.mock('@/modules/auth/presentation/view-model/redux/action', () => () => {
  const actual = jest.requireActual('@/modules/auth/presentation/view-model/redux/action');
  return {
    ...actual.default(),
    requestLogout: requestLogoutMock,
  };
});

const Component = (
  <ReduxProvider store={store}>
    <BrowserRouter>
      <Routes>
        <Route key="sidebar" path="/" element={<SideBar />} />
      </Routes>
    </BrowserRouter>
  </ReduxProvider>
);

describe('Sidebar Component Test Group', () => {
  afterEach(() => {
    cleanup();
  });
  // 1. Component Mount
  it('1. Component Mount', async () => {
    render(Component, { wrapper: ThemeProvider });
    const permission = permissionMock.concat(['/']);
    store.dispatch(
      authActions.setPermission(convertPermissionArrayToPermissionTree([...permission])),
    );
    await act(async () => {
      const list = await screen.findByText(/^customer$/i);
      expect(list).toBeVisible();
    });
  });
  // 2. Expand List
  it('2. Expand List', async () => {
    render(Component, { wrapper: ThemeProvider });
    const list = screen.getByText(/^customer$/i);
    fireEvent.click(list);
    await act(async () => {
      const childList = await screen.findByText(/customer details/i);
      expect(childList).toBeVisible();
    });
  });
  // 3. Hide Sidebar
  it('3. Hide Sidebar', async () => {
    render(Component, { wrapper: ThemeProvider });
    const hide = screen.getByTestId('hide');
    fireEvent.click(hide);
    await act(async () => {
      const childList = await screen.findByTestId(/hidden/i);
      expect(childList).toBeTruthy();
    });
  });
  // 4. Logout
  it('4. Logout', async () => {
    render(Component, { wrapper: ThemeProvider });
    const logout = screen.getByText(/^logout$/i);
    fireEvent.click(logout);
    expect(requestLogoutMock).toBeCalled();
  });
});
