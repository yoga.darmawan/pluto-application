import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import VerifiedUserOutlinedIcon from '@mui/icons-material/VerifiedUserOutlined';
import SettingsApplicationsOutlinedIcon from '@mui/icons-material/SettingsApplicationsOutlined';
import ContentInboxOutlinedIcon from '@mui/icons-material/InboxOutlined';
import PublishOutlinedIcon from '@mui/icons-material/PublishOutlined';
import BuildOutlinedIcon from '@mui/icons-material/BuildOutlined';
import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import PanToolOutlinedIcon from '@mui/icons-material/PanToolOutlined';
import StorefrontIcon from '@mui/icons-material/Storefront';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';

import { stringPermission } from '@/shared/utils/permission';
import { Menu as MenuType } from '@/shared/core/redux/types';

const Menu: MenuType[] = [
  {
    key: stringPermission.customer,
    path: '/customer',
    name: 'Customer',
    icon: <PeopleAltOutlinedIcon />,
    children: [
      {
        key: stringPermission.customerDetailList,
        path: '/customer/customer-details',
        name: 'Customer Details',
      },
      {
        key: stringPermission.customerTransactionList,
        path: '/customer/customer-transaction',
        name: 'Customer Transaction',
      },
      {
        key: stringPermission.eksekusiApproval,
        path: '/customer/execution-approval',
        name: 'Eksekusi Approval',
      },
    ],
  },
  {
    key: stringPermission.kyc,
    path: '/account-verification',
    name: 'Account Verification',
    icon: <VerifiedUserOutlinedIcon />,
    children: [
      {
        key: stringPermission.kycList,
        path: '/account-verification',
        name: 'Account Verification Approval',
      },
    ],
  },
  {
    key: stringPermission.settings,
    path: '/management',
    name: 'Settings',
    icon: <SettingsApplicationsOutlinedIcon />,
    children: [
      {
        key: stringPermission.rolePermissionList,
        path: '/management/role',
        name: 'Manage Role',
      },
      {
        key: stringPermission.userList,
        path: '/management/user',
        name: 'Manage Users',
      },
    ],
  },
  // [NOTE || WARNING]: NEED TO BE CHANGED TO PROPER ROUTE KEY AFTER BE DEPLOY TO DEV
  {
    key: stringPermission.blacklist,
    path: '/blacklist-management',
    name: 'Blacklist Management',
    icon: <PanToolOutlinedIcon />,
    children: [
      {
        key: stringPermission.blacklistRequestList,
        path: '/blacklist/create',
        name: 'Create Request',
      },
      {
        key: stringPermission.blacklistDataList,
        path: '/blacklist/data',
        name: 'Blacklist Data',
      },
      {
        key: stringPermission.blacklistApprovalList,
        path: '/blacklist/approval',
        name: 'Approval',
      },
      {
        key: stringPermission.blacklistHistoryList,
        path: '/blacklist/history',
        name: 'History',
      },
    ],
  },
  {
    key: stringPermission.manualTransactionPosting,
    path: '/manual-transaction-posting',
    name: 'Manual Transaction Posting',
    icon: <PublishOutlinedIcon />,
    children: [
      {
        key: stringPermission.manualTransactionPostingCustomerList,
        path: '/manual-transaction-posting/customer/list',
        name: 'Customer',
      },
      {
        key: stringPermission.manualTransactionPostingMerchantList,
        path: '/manual-transaction-posting/merchant/list',
        name: 'Merchant',
      },
      {
        key: stringPermission.manualTransactionPostingApprovalList,
        path: '/manual-transaction-posting/approval/list',
        name: 'Approval',
      },
      {
        key: stringPermission.manualTransactionPostingHistorylList,
        path: '/manual-transaction-posting/history/list',
        name: 'History',
      },
    ],
  },
  {
    key: stringPermission.product,
    path: '/product-management',
    name: 'Product Management',
    icon: <BuildOutlinedIcon />,
    children: [
      {
        key: stringPermission.productList,
        path: '/product-management/products',
        name: 'Product',
      },
      {
        key: stringPermission.partnerList,
        path: '/product-management/partner',
        name: 'Partner',
      },
      {
        key: stringPermission.productGroupList,
        path: '/product-management/product-group',
        name: 'Product Group',
      },
      {
        key: stringPermission.productSkuList,
        path: '/product-management/product-sku',
        name: 'Product SKU',
      },
      {
        key: stringPermission.productPartnerList,
        path: '/product-management/product-partner',
        name: 'Product Partner',
      },
    ],
  },
  {
    key: stringPermission.report,
    path: '/report',
    name: 'Reporting Management',
    icon: <DescriptionOutlinedIcon />,
    children: [
      {
        key: stringPermission.reportBillerDetailView,
        path: '/report/biller-report',
        name: 'Biller Detail Report',
      },
      {
        key: stringPermission.reportBillerEodView,
        path: '/report/eod-report',
        name: 'Biller EoD Report',
      },
    ],
  },
  {
    key: stringPermission.contentDelivery,
    path: '/content-delivery',
    name: 'Content',
    icon: <ContentInboxOutlinedIcon />,
    children: [
      {
        key: stringPermission.contentDeliveryServiceList,
        path: '/content-delivery/list',
        name: 'Content Delivery',
      },
    ],
  },
  {
    key: stringPermission.paymentLink,
    path: '/payment-link',
    name: 'Merchant',
    icon: <StorefrontIcon />,
    children: [
      {
        key: stringPermission.paymentLinkList,
        path: '/payment-link/list',
        name: 'Payment Link',
      },
      {
        key: stringPermission.merchantTransactionList,
        path: '/merchant/merchant-transaction',
        name: 'Merchant Transactions',
      },
    ],
  },
  {
    key: stringPermission.partnerManagement,
    path: '/partner-management',
    name: 'Partner Management',
    icon: <AssignmentIndIcon />,
    children: [
      {
        key: stringPermission.partnerManagementMerchantList,
        path: '/partner-management/merchant/list',
        name: 'Merchant',
      },
    ],
  },
];

export default Menu;
