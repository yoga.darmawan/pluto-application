import { SxProps } from '@mui/material';
import styled from '@emotion/styled';

export const drawerPaperSx: SxProps = {
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
  '&::-webkit-scrollbar': { display: 'none' },
  width: ({ customComponent: { sidebar } }: any) => sidebar.width,
};

export const toolbarSx: SxProps = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  px: [1],
  img: {
    width: '54px',
  },
};

export const listSx: SxProps = {
  flex: 1,
  overflowY: 'scroll',
  '&::-webkit-scrollbar': { display: 'none' },
};

export const listItemSx: SxProps = {
  padding: 0,
  borderBottom: '1px solid',
  borderColor: 'mist.light',
};

export const listItemTextSx = { fontWeight: 500 };

export const listIconSx: SxProps = { minWidth: '35px' };

export const listIconChevronSx: SxProps = { minWidth: '20px', marginLeft: '10px' };

export const listChildButtonSx: SxProps = { paddingLeft: '20px' };

export const listChildSx: SxProps = {
  padding: 0,
};

export const listChildItemSx: SxProps = {
  padding: 0,
  borderBottom: '1px solid',
  borderColor: 'mist.light',
};

export const LogoutRow = styled('div')`
  width: ${({
    theme: {
      customComponent: { sidebar },
    },
  }) => `${sidebar.width}px`};
  display: flex;
  align-items: center;
  padding: 1.5rem;
  box-sizing: border-box;
  .MuiSvgIcon-root {
    margin-right: 10px;
  }
  background-color: ${({
    theme: {
      palette: { common },
    },
  }) => common.white};
`;
