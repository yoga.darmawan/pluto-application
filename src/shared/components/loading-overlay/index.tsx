import React from 'react';
import Modal from '@mui/material/Modal';
import CircularProgress from '@mui/material/CircularProgress';
import DataModel from '@/core/redux/data';

import Style from './style';

const OverlayLoading = () => {
  const {
    loading: { overlay },
  } = DataModel();
  return (
    <Modal open={overlay}>
      <Style>
        <CircularProgress sx={{ color: 'primary.main' }} />
      </Style>
    </Modal>
  );
};

export default React.memo(OverlayLoading);
