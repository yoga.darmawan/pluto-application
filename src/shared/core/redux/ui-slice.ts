import { createSlice } from '@reduxjs/toolkit';

import { InitialState, Loading, Config } from './types';

const initialState: InitialState = {
  loading: {
    overlay: false,
    primaryAction: false,
  },
  config: {},
  sidebar: {
    open: true,
  },
};

// [NOTE]: bugs https://github.com/microsoft/TypeScript/issues/42873
// temporary fix: adding pnpm override immer and install immer in package.json
// also specify path to immer in tsconfig.json
const uiSlice: any = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    triggerLoading(state, { payload }) {
      state.loading = { ...state.loading, ...(payload as Loading) };
    },
    setConfig(state, { payload }) {
      state.config = { ...state.config, ...(payload as Config) };
    },
    triggerSidebar(state, { payload }) {
      state.sidebar = { ...state.sidebar, ...(payload as { open: boolean }) };
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice;
