export interface Loading {
  overlay?: boolean;
  primaryAction?: boolean;
}

export type LoadingParam = keyof Loading;

export interface DatadogErrorLog {
  is_show_error_one_time_payment?: string;
  is_show_error_payment_at_partner?: string;
}

export interface Config {
  firebase_config?: {
    [key: string]: string;
  };
  datadog_config?: {
    [key: string]: string;
  };
}

export interface Sidebar {
  open: boolean;
}

export interface Menu {
  key: string;
  path: string;
  name: string;
  icon: any;
  children?: {
    key: string;
    path: string;
    name: string;
  }[];
}

export interface InitialState {
  loading: Loading;
  config: Config;
  sidebar: Sidebar;
}
