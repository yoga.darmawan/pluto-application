import { useSelector } from 'react-redux';

import { RootState } from '@/core/redux/store';

export default function DataModel() {
  const loading = useSelector((state: RootState) => state.ui.loading);

  const sidebar = useSelector((state: RootState) => state.ui.sidebar);

  return {
    loading,
    sidebar,
  };
}
