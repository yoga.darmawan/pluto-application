import { PathParams } from '@/domains/general/entity';

export const getHomeLogin = () => '/';

export const getResetPassword = () => '/reset-password';

export const generatePath = ({ name }: PathParams): string => {
  let url = '';
  switch (name) {
    case 'HomeLogin':
      url = getHomeLogin();
      break;
    case 'ResetPassword':
      url = getResetPassword();
      break;
    default:
      break;
  }
  return url;
};
