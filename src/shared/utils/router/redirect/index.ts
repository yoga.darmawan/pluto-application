import React from 'react';
import { useNavigate } from 'react-router-dom';
import { stringifyUrl } from 'query-string';

import { PathParams } from '@/domains/general/entity';

import { generatePath } from '../path';

export const useRedirect = () => {
  const navigate = useNavigate();

  const redirect = React.useCallback(
    ({ name, id, query, url, pushType = 'push' }: PathParams): void => {
      let path = '';
      if (url) {
        path = url;
      } else {
        path = generatePath({ name, id, query });
      }
      navigate(path, {
        replace: pushType === 'replace',
      });
    },
    [],
  );

  const goBack = React.useCallback(() => {
    navigate(-1);
  }, []);

  const locationRedirect = React.useCallback((url: string) => {
    window.location.href = stringifyUrl({ url });
  }, []);

  return {
    redirect,
    locationRedirect,
    goBack,
  };
};
