import React from 'react';

import { PermissionNode } from '@/domains/auth/entity';
import AuthViewModel from '@/modules/auth/presentation/view-model';

export const convertPermissionArrayToPermissionTree = (data: string[]): PermissionNode => {
  const result: PermissionNode[] = [];
  data.forEach((s) =>
    s.split('.').reduce(
      (object: PermissionNode, value: string) => {
        // eslint-disable-next-line no-param-reassign
        let item = (object.children = object.children || []).find((q) => q.value === value);
        if (!item) {
          object.children.push((item = { value }));
        }
        return item;
      },
      { children: result },
    ),
  );

  return {
    children: result,
  };
};

export const isPermissionValid = (
  permissionTree: PermissionNode,
  permission: string,
  excludedPermissions: string[],
): boolean => {
  // check if permission is not valid
  if (permission === undefined) {
    return false;
  }

  // check is permissionTree is ready
  if (Object.keys(permissionTree).length === 0) {
    return false;
  }

  // check is excluded permission
  const isPermissionExcluded = excludedPermissions.indexOf(permission);
  if (isPermissionExcluded > -1) {
    return true;
  }

  const permissionFragments = permission.split('.');
  let currentpermissionTree = permissionTree;

  // eslint-disable-next-line no-restricted-syntax
  for (const permissionFragment of permissionFragments) {
    // eslint-disable-next-line no-loop-func
    const currentPermissionNode = currentpermissionTree.children.filter(
      // eslint-disable-next-line @typescript-eslint/no-loop-func
      (item: PermissionNode) => {
        if (item.value === permissionFragment) {
          currentpermissionTree = item;
        }

        return item.value === permissionFragment;
      },
    );

    if (currentPermissionNode.length === 0) {
      return false;
    }
  }

  return true;
};

export const usePermission = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { actionModel, dataModel } = auth;
  const { requestPermission } = actionModel();
  const { permission } = dataModel();

  const init = React.useCallback(() => {
    requestPermission();
  }, []);

  const findPermission = React.useCallback(
    (string: string): boolean => {
      return isPermissionValid(permission, string, ['/']);
    },
    [permission],
  );

  return {
    init,
    findPermission,
  };
};
