/* eslint-disable import/prefer-default-export */
export const stringPermission = {
  // Customer
  customer: 'customer',

  // Customer Details
  customerDetailList: 'customer.customer-details.list',
  customerDetailCustomerBlock: 'customer.customer-details.customer-block',
  customerDetailCustomerUnblock: 'customer.customer-details.customer-unblock',
  customerDetailDeviceBlock: 'customer.customer-details.device-block',
  customerDetailDeviceUnblock: 'customer.customer-details.device-unblock',
  customerDetailLinkageBlock: 'customer.customer-details.partner-linkage-block',
  customerDetailLinkageUnblock: 'customer.customer-details.partner-linkage-unblock',
  customerDetailPfmInfo: 'customer.customer-details.pfm-info',

  // Customer Transaction
  customerTransactionList: 'customer.customer-transaction.list',
  customerTransactionDetail: 'customer.customer-transaction.detail',
  customerTransactionQrisMpmDetail: 'customer.customer-transaction.mpm-detail',
  customerTransactionQrisMpmChangeStatus: 'customer.customer-transaction.mpm-change-status',

  // Order Detail
  orderDetailList: 'order.order-detail.information',
  orderDetailCheckStatus: 'order.order-detail.check-status',
  orderDetailChangeStatus: 'order.order-detail.change-status',

  // Eksekusi Approval
  eksekusiApproval: 'customer.eksekusi-approval',
  eksekusiApprovalCustomerBlock: 'customer.eksekusi-approval.customer-block',
  eksekusiApprovalCustomerUnblock: 'customer.eksekusi-approval.customer-unblock',
  eksekusiApprovalDeviceBlock: 'customer.eksekusi-approval.device-block',
  eksekusiApprovalDeviceUnblock: 'customer.eksekusi-approval.device-unblock',
  eksekusiApprovalLinkageBlock: 'customer.eksekusi-approval.partner-block',
  eksekusiApprovalLinkageUnblock: 'customer.eksekusi-approval.partner-unblock',

  // KYC
  kyc: 'account-verification',
  kycList: 'account-verification.account-verification-request.list',
  kycDetail: 'account-verification.account-verification-request.review',
  kycReject: 'account-verification.account-verification-request.reject',
  kycSave: 'account-verification.account-verification-request.save',
  kycApprove: 'account-verification.account-verification-request.approve',

  // Settings
  settings: 'settings',

  // Role
  rolePermissionList: 'settings.manage-role.list',
  rolePermissionCreate: 'settings.manage-role.create',
  rolePermissionEdit: 'settings.manage-role.edit',
  rolePermissionDelete: 'settings.manage-role.delete',

  // User
  userList: 'settings.manage-users.list',
  userCreate: 'settings.manage-users.create',
  userEdit: 'settings.manage-users.edit',
  userBlock: 'settings.manage-users.block',
  userUnblock: 'settings.manage-users.unblock',
  userDeactivate: 'settings.manage-users.deactivate',
  userReinvite: 'settings.manage-users.reinvite',

  // Manual Transaction Posting
  manualTransactionPosting: 'manual-transaction-posting',
  manualTransactionPostingCustomerList: 'manual-transaction-posting.customer.list',
  manualTransactionPostingMerchantList: 'manual-transaction-posting.merchant.list',
  manualTransactionPostingApprovalList: 'manual-transaction-posting.approval.list',
  manualTransactionPostingApprovalApprove: 'manual-transaction-posting.approval.approve',
  manualTransactionPostingApprovalReject: 'manual-transaction-posting.approval.reject',
  manualTransactionPostingHistorylList: 'manual-transaction-posting.transaction-history.list',
  manualTransactionPostingHistorylDownload:
    'manual-transaction-posting.transaction-history.download',
  manualTransactionPostingHistorylFailReportList: 'manual-transaction-posting.fail-report.list',
  manualTransactionPostingHistoryFailReportDownload:
    'manual-transaction-posting.fail-report.download',

  // Product Management
  product: 'product-management',

  // Product
  productList: 'product-management.product.list',

  // Partner
  partnerList: 'product-management.partner.list',

  // Product Group
  productGroupList: 'product-management.product-group.list',
  productGroupUpload: 'product-management.product-group.upload',
  productGroupDownload: 'product-management.product-group.download',

  // Product SKU
  productSkuList: 'product-management.product-sku.list',
  productSkuUpload: 'product-management.product-sku.upload',
  productSkuDownload: 'product-management.product-sku.download',

  // Product Partner
  productPartnerList: 'product-management.product-partner.list',
  productPartnerUpload: 'product-management.product-partner.upload',
  productPartnerDownload: 'product-management.product-partner.download',

  // Reporting
  report: 'reporting',

  // Biller Detail Report
  reportBillerDetailView: 'reporting.biller-detail-report.view-and-selection',
  reportBillerDetailDownload: 'reporting.biller-detail-report.download',

  // Biller EOD Report
  reportBillerEodView: 'reporting.eod-biller-report.view-and-selection',
  reportBillerEodDownload: 'reporting.eod-biller-report.download',

  // Content Delivery Service
  contentDelivery: 'content-delivery',
  contentDeliveryServiceList: 'content-delivery.list',
  contentDeliveryServiceDetail: 'content-delivery.detail',
  contentDeliveryServiceCreate: 'content-delivery.create',
  contentDeliveryServiceApprove: 'content-delivery.approve',
  contentDeliveryServiceReject: 'content-delivery.reject',
  contentDeliveryServiceCancel: 'content-delivery.cancel',

  // Customer Blacklist
  blacklist: 'blacklist',

  // Customer Blacklist Request
  blacklistRequestList: 'blacklist.request.list',
  blacklistRequestCreate: 'blacklist.request.create',
  blacklistRequestDownloadFile: 'blacklist.request.download.file',

  // Customer Blacklist Data
  blacklistDataList: 'blacklist.data.list',
  blacklistDataDetail: 'blacklist.data.detail',
  blacklistDataUpdate: 'blacklist.data.update',
  blacklistDataRelease: 'blacklist.data.release',
  blacklistDataDownloadSearchResult: 'blacklist.data.download.search.result',
  blacklistDataDownload: 'blacklist.data.download.search.result',

  // Customer Blacklist Approval
  blacklistApprovalList: 'blacklist.approval.list',
  blacklistApprovalApprove: 'blacklist.approval.approve',
  blacklistApprovalReject: 'blacklist.approval.reject',

  // Customer Blacklist History
  blacklistHistoryList: 'blacklist.history.list',

  // Merchant Payment Link
  paymentLink: 'merchant.payment-link',
  paymentLinkList: 'merchant.payment-link.list',
  paymentLinkDetail: 'merchant.payment-link.view',
  merchantTransactionList: 'merchant.merchant-transaction.list',
  merchantTransactionDetail: 'merchant.merchant-transaction.detail',

  // Partner Management
  partnerManagement: 'partner-management',
  partnerManagementMerchantList: 'partner-management.merchant.list',
  partnerManagementMerchantDetail: 'partner-management.merchant.detail',
  partnerManagementMerchantEdit: 'partner-management.merchant.edit',
  partnerManagementMerchantStoreList: 'partner-management.merchant.store.list',
  partnerManagementMerchantStoreDetail: 'partner-management.merchant.store.detail',
};
