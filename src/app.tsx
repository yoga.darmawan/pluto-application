import { Provider as ReduxProvider } from 'react-redux';

import { store } from '@/core/redux/store';
import { ThemeProvider } from '@/theme/index';
import Pages from './pages';

export default function App() {
  return (
    <ReduxProvider store={store}>
      <ThemeProvider>
        <Pages />
      </ThemeProvider>
    </ReduxProvider>
  );
}
