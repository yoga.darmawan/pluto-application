import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import AuthViewModel from '@/modules/auth/presentation/view-model';
import LoadingOverlay from '@/shared/components/loading-overlay';

import { Publics, Privates } from './routes';

const Pages = () => {
  // INIT VIEW MODEL
  const auth = (() => AuthViewModel())();
  const { dataModel } = auth;
  const { isLoggedin } = dataModel();

  return (
    <BrowserRouter>
      <LoadingOverlay />
      {isLoggedin ? (
        <>
          <Privates />
        </>
      ) : (
        <Publics />
      )}
    </BrowserRouter>
  );
};

export default Pages;
