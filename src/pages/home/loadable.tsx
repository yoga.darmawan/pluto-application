import { loadable } from '@/shared/utils/loadable';

const HomePage = loadable(() => import('./index'));

const config = {
  path: '/',
  key: 'home-page',
  component: <HomePage />,
};

export default config;
