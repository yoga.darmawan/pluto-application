import { loadable } from '@/shared/utils/loadable';

const ManagementPage = loadable(() => import('./index'));

const config = {
  path: '/management/*',
  key: 'management-page',
  component: <ManagementPage />,
};

export default config;
