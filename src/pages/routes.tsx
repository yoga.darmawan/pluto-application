import { Routes, Route } from 'react-router-dom';

import HomePage from './home/loadable';
import LoginPage from './login/loadable';
import ManagementPage from './management/loadable';
import ResetPassword from './reset-password/loadable';
import PageLayout from '@/shared/components/layout';

const generateRoutes = (pages) => {
  return (
    <>
      {pages.map(({ key, path, component }) => (
        <Route key={key} path={path} element={component} />
      ))}
    </>
  );
};

export const Privates = () => {
  return (
    <Routes>
      <Route key="not-found" path="*" element={<div>Page not found</div>} />
      <Route element={<PageLayout />}>{generateRoutes([HomePage, ManagementPage])}</Route>
    </Routes>
  );
};

export const Publics = () => {
  return (
    <>
      <Routes>
        <Route key="not-found" path="*" element={<div>Page not found</div>} />
        <Route>{generateRoutes([LoginPage, ResetPassword])}</Route>
      </Routes>
    </>
  );
};
