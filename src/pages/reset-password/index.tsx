import React from 'react';

import ResetPassword from '@/modules/auth/presentation/interface/reset-password';

const ResetPasswordPage = () => {
  return <ResetPassword />;
};

export default ResetPasswordPage;
