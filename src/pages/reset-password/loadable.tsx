import { loadable } from '@/shared/utils/loadable';

import { getResetPassword } from '@/shared/utils/router';

const ResetPasswordPage = loadable(() => import('./index'));

const config = {
  path: getResetPassword(),
  key: 'reset-password-page',
  component: <ResetPasswordPage />,
};

export default config;
