import { loadable } from '@/shared/utils/loadable';

const LoginPage = loadable(() => import('./index'));

const config = {
  path: '/',
  key: 'login-page',
  component: <LoginPage />,
};

export default config;
