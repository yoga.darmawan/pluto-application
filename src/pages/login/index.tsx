import React from 'react';

import Login from '@/modules/auth/presentation/interface/login';

const LoginPage = () => {
  return <Login />;
};

export default LoginPage;
